---
title: S/MIME Example Keys and Certificates
docname: draft-ietf-lamps-samples-08
category: info

ipr: trust200902
area: int
workgroup: lamps
keyword: Internet-Draft

stand_alone: yes

venue:
  group: "LAMPS"
  type: "Working Group"
  mail: "spasm@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/spasm/"
  repo: "https://gitlab.com/dkg/lamps-samples"
  latest: "https://dkg.gitlab.io/lamps-samples/"

author:
 -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    street: 125 Broad St.
    city: New York, NY
    code: 10004
    country: USA
    abbrev: ACLU
    email: dkg@fifthhorseman.net
    role: editor
informative:
 FIPS186-4: DOI.10.6028/NIST.FIPS.186-4
 I-D.bre-openpgp-samples:
 RFC4134:
 RFC5322:
 RFC7469:
 RFC8410:
 RFC8418:
 SHA: DOI.10.6028/NIST.FIPS.180-4
 TEST-POLICY:
  target: https://csrc.nist.gov/CSRC/media/Projects/Computer-Security-Objects-Register/documents/test_policy.pdf
  title: "Test Certificate Policy to Support PKI Pilots and Testing"
  author:
   org: NIST - Computer Security Divisiion (CSD)
  date: 2012-05
normative:
 RFC5280:
 RFC5958:
 RFC7292:
 RFC7468:
 RFC8032:
 RFC8479:
 RFC8551:
--- abstract

The S/MIME development community benefits from sharing samples of signed or encrypted data. This document facilitates such collaboration by defining a small set of X.509v3 certificates and keys for use when generating such samples.

--- middle

# Introduction

The S/MIME ({{RFC8551}}) development community, in particular the email development community, benefits from sharing samples of signed and/or encrypted data.
Often, the exact key material used does not matter because the properties being tested pertain to implementation correctness, completeness, or interoperability of the overall system.
However, without access to the relevant secret key material, a sample is useless.

This document defines a small set of X.509v3 certificates ({{RFC5280}}) and secret keys for use when generating or operating on such samples.

An example RSA Certification Authority is supplied, and sample RSA certificates are provided for two "personas", Alice and Bob.

Additionally, an Ed25519 ({{RFC8032}}) Certification Authority is supplied, along with sample Ed25519 certificates for two more "personas", Carlos and Dana.

This document focuses narrowly on functional, well-formed identity and key material.
It is a starting point that other documents can use to develop sample signed or encrypted messages, test vectors, or other artifacts for improved interoperability.

## Terminology

"Certification Authority" (or "CA")
: a party capable of issuing X.509 certificates

"End-Entity" (or "EE")
: a party that is capable of using X.509 certificates (and their corresponding secret key material) 

"Mail User Agent" (or "MUA")
: is a program that generates or handles email messages ({{RFC5322}}).

## Prior Work

{{RFC4134}} contains some sample certificates as well as messages of various S/MIME formats.
That older work has unacceptably old algorithm choices that may introduce failures when testing modern systems: in 2019, some tools explicitly marked 1024-bit RSA and 1024-bit DSS as weak.

This earlier document also does not use the now widely accepted Privacy-Enhanced Mail (PEM) encoding (see {{RFC7468}}) for the objects and instead embeds runnable Perl code to extract them from the document.

It also includes examples of messages and other structures that are greater in ambition than this document intends to be.

{{RFC8410}} includes an example X25519 certificate that is certified with Ed25519, but it appears to be self issued, and it is not directly useful in testing an S/MIME-capable MUA.

# Background

## Certificate Usage

These X.509 certificates ({{RFC5280}}) are designed for use with S/MIME protections ({{RFC8551}}) for email ({{RFC5322}}).

In particular, they should be usable with signed and encrypted messages as part of test suites and interoperability frameworks.

All end-entity and intermediate CA certificates are marked with Certificate Policies from {{TEST-POLICY}} indicating that they are intended only for use in testing environments.
End-entity certificates are marked with policy 2.16.840.1.101.3.2.1.48.1 and intermediate CAs are marked with policy  2.16.840.1.101.3.2.1.48.2.

## Certificate Expiration

The certificates included in this document expire in 2052.
This should be sufficiently far in the future that they will be useful for a few decades.
However, when testing tools in the far future (or when playing with clock-skew scenarios), care should be taken to consider the certificate validity window.

Due to this lengthy expiration window, these certificates will not be particularly useful to test or evaluate the interaction between certificate expiration and protected messages.

## Certificate Revocation

Because these are expected to be used in test suites or examples, and we do not expect there to be online network services in these use cases, we do not expect these certificates to produce any revocation artifacts.

As a result, none of the certificates include either an Online Certificate Status Protocol (OCSP) indicator (see `id-ad-ocsp` as defined in the Authority Information Access X.509 extension in Section 4.2.2.1 of {{RFC5280}}) or a Certificate Revocation List (CRL) indicator (see the CRL Distribution Points X.509 extension as defined in Section 4.2.1.13 of {{RFC5280}}).

## Using the CA in Test Suites

To use these end-entity certificates in a piece of software (for example, in a test suite or an interoperability matrix), most tools will need to accept either the example RSA CA ({{sample-rsa-ca}}) or the example Ed25519 CA ({{sample-ed25519-ca}}) as a legitimate root authority.

Note that some tooling behaves differently for certificates validated by "locally installed root CAs" than for pre-installed "system-level" root CAs).
For example, many common implementations of HTTP Public Key Pinning (HPKP) ({{RFC7469}}) only applied the designed protections when dealing with a certificate issued by a pre-installed "system-level" root CA and were disabled when dealing with a certificate issued by a "locally installed root CA".

To test some tooling specifically, it may be necessary to install the root CA as a "system-level" root CA.

## Certificate Chains

In most real-world examples, X.509 certificates are deployed with a chain of more than one X.509 certificate.
In particular, there is typically a long-lived root CA that users' software knows about upon installation, and the end-entity certificate is issued by an intermediate CA, which is in turn issued by the root CA.

The example end-entity certificates in this document can be used either with a simple two-link certificate chain (they are directly certified by their corresponding root CA) or in a three-link chain.

For example, Alice's encryption certificate (`alice.encrypt.crt`; see {{alice-encrypt-cert}}) can be validated by a peer that directly trusts the example RSA CA's root cert (`ca.rsa.crt`; see {{rsa-ca-cert}}):


{: artwork-name="alice-validate-two-hops"}
~~~
╔════════════╗  ┌───────────────────┐
║ ca.rsa.crt ╟─→│ alice.encrypt.crt │
╚════════════╝  └───────────────────┘
~~~

And it can also be validated by a peer that only directly trusts the example Ed25519 CA's root cert (`ca.25519.crt`; see {{ed25519-ca-cert}}) via an intermediate cross-signed CA cert (`ca.rsa.cross.crt`; see {{rsa-ca-cross-cert}}):

{: artwork-name="alice-validate-three-hops"}
~~~
╔══════════════╗  ┌──────────────────┐  ┌───────────────────┐
║ ca.25519.crt ╟─→│ ca.rsa.cross.crt ├─→│ alice.encrypt.crt │
╚══════════════╝  └──────────────────┘  └───────────────────┘
~~~

By omitting the cross-signed CA certs, it should be possible to test a "transvalid" certificate (an end-entity certificate that is supplied without its intermediate certificate) in some configurations.

## Passwords

Each secret key presented in this document is represented as a PEM-encoded PKCS #8 ({{RFC5958}}) object in cleartext form (it has no password).

As such, the secret key objects are not suitable for verifying interoperable password protection schemes.

However, the PKCS #12 ({{RFC7292}}) objects do have simple textual passwords, because tooling for dealing with passwordless PKCS #12 objects is underdeveloped at the time of this document.

## Secret Key Origins

The secret RSA keys in this document are all deterministically derived using provable prime generation as found in {{FIPS186-4}} based on known seeds derived via SHA-256 ({{SHA}}) from simple strings.
The validation parameters for these derivations are stored in the objects themselves as specified in {{RFC8479}}.

The secret Ed25519 and X25519 keys in this document are all derived by hashing a simple string.
The seeds and their derivation are included in the document for informational purposes and to allow re-creation of the objects from appropriate tooling.

All RSA seeds used are 224 bits long (the first 224 bits of the SHA-256 digest of the origin string) and are represented in hexadecimal.

# Example RSA Certification Authority {#sample-rsa-ca}

The example RSA Certification Authority has the following information:

 - Name: `Sample LAMPS RSA Certification Authority`

## RSA Certification Authority Root Certificate {#rsa-ca-cert}

This certificate is used to verify certificates issued by the example RSA Certification Authority.

{: artwork-name="ca.rsa.crt"}
~~~ application/x-x509-ca-cert
{::include ca.rsa.crt}
~~~

## RSA Certification Authority Secret Key

This secret key material is used by the example RSA Certification Authority to issue new certificates.

{: artwork-name="ca.rsa.key"}
~~~ application/x-pem-file
{::include ca.rsa.key}
~~~

This secret key was generated using provable prime generation found in {{FIPS186-4}} using the seed `@@ca.rsa.seed@@`.
This seed is the first 224 bits of the SHA-256 ({{SHA}}) digest of the string `@@ca.rsa.seed-source@@`.

## RSA Certification Authority Cross-Signed Certificate {#rsa-ca-cross-cert}

If an email client only trusts the Ed25519 Certification Authority Root Certificate found in {{ed25519-ca-cert}}, they can use this intermediate CA certificate to verify any end-entity certificate issued by the example RSA Certification Authority.

{: artwork-name="ca.rsa.cross.crt"}
~~~ application/x-x509-ca-cert
{::include ca.rsa.cross.crt}
~~~

# Alice's Sample Certificates

Alice has the following information:

 - Name: `Alice Lovelace`
 - email Address: `alice@smime.example`

## Alice's Signature Verification End-Entity Certificate {#alice-verify-cert}

This certificate is used for verification of signatures made by Alice.

{: artwork-name="alice.sign.crt"}
~~~ application/x-pem-file
{::include alice.sign.crt}
~~~

## Alice's Signing Private Key Material {#alice-sign-key}

This private key material is used by Alice to create signatures.

{: artwork-name="alice.sign.key"}
~~~ application/x-pem-file
{::include alice.sign.key}
~~~

This secret key was generated using provable prime generation found in {{FIPS186-4}} using the seed `@@alice.sign.seed@@`.
This seed is the first 224 bits of the SHA-256 ({{SHA}}) digest of the string `@@alice.sign.seed-source@@`.

## Alice's Encryption End-Entity Certificate {#alice-encrypt-cert}

This certificate is used to encrypt messages to Alice.

{: artwork-name="alice.encrypt.crt"}
~~~ application/x-pem-file
{::include alice.encrypt.crt}
~~~

## Alice's Decryption Private Key Material {#alice-decrypt-key}

This private key material is used by Alice to decrypt messages.

{: artwork-name="alice.encrypt.key"}
~~~ application/x-pem-file
{::include alice.encrypt.key}
~~~

This secret key was generated using provable prime generation found in {{FIPS186-4}} using the seed `@@alice.encrypt.seed@@`.
This seed is the first 224 bits of the SHA-256 ({{SHA}}) digest of the string `@@alice.encrypt.seed-source@@`.

## PKCS #12 Object for Alice

This PKCS #12 ({{RFC7292}}) object contains the same information as presented in {{rsa-ca-cross-cert}}, {{alice-verify-cert}}, {{alice-sign-key}}, {{alice-encrypt-cert}}, and {{alice-decrypt-key}}.

It is locked with the simple five-letter password `alice`.

{: artwork-name="alice.p12"}
~~~ application/x-pem-file
{::include alice.p12}
~~~

# Bob's Sample

Bob has the following information:

 - Name: `Bob Babbage`
 - email Address: `bob@smime.example`

## Bob's Signature Verification End-Entity Certificate {#bob-verify-cert}

This certificate is used for verification of signatures made by Bob.

{: artwork-name="bob.sign.crt"}
~~~ application/x-pem-file
{::include bob.sign.crt}
~~~

## Bob's Signing Private Key Material {#bob-sign-key}

This private key material is used by Bob to create signatures.

{: artwork-name="bob.sign.key"}
~~~ application/x-pem-file
{::include bob.sign.key}
~~~

This secret key was generated using provable prime generation found in {{FIPS186-4}} using the seed `@@bob.sign.seed@@`.
This seed is the first 224 bits of the SHA-256 ({{SHA}}) digest of the string `@@bob.sign.seed-source@@`.

## Bob's Encryption End-Entity Certificate {#bob-encrypt-cert}

This certificate is used to encrypt messages to Bob.

{: artwork-name="bob.encrypt.crt"}
~~~ application/x-pem-file
{::include bob.encrypt.crt}
~~~

## Bob's Decryption Private Key Material {#bob-decrypt-key}

This private key material is used by Bob to decrypt messages.

{: artwork-name="bob.encrypt.key"}
~~~ application/x-pem-file
{::include bob.encrypt.key}
~~~

This secret key was generated using provable prime generation found in {{FIPS186-4}} using the seed `@@bob.encrypt.seed@@`.
This seed is the first 224 bits of the SHA-256 ({{SHA}}) digest of the string `@@bob.encrypt.seed-source@@`.

## PKCS #12 Object for Bob

This PKCS #12 ({{RFC7292}}) object contains the same information as presented in {{rsa-ca-cross-cert}}, {{bob-verify-cert}}, {{bob-sign-key}}, {{bob-encrypt-cert}}, and {{bob-decrypt-key}}.

It is locked with the simple three-letter password `bob`.

{: artwork-name="bob.p12"}
~~~ application/x-pem-file
{::include bob.p12}
~~~

# Example Ed25519 Certification Authority {#sample-ed25519-ca}

The example Ed25519 Certification Authority has the following information:

 - Name: `Sample LAMPS Ed25519 Certification Authority`

## Ed25519 Certification Authority Root Certificate {#ed25519-ca-cert}

This certificate is used to verify certificates issued by the example Ed25519 Certification Authority.

{: artwork-name="ca.25519.crt"}
~~~ application/x-x509-ca-cert
{::include ca.25519.crt}
~~~

## Ed25519 Certification Authority Secret Key

This secret key material is used by the example Ed25519 Certification Authority to issue new certificates.

{: artwork-name="ca.25519.key"}
~~~ application/x-pem-file
{::include ca.25519.key}
~~~

This secret key is the SHA-256 ({{SHA}}) digest of the ASCII string `@@ca.25519.seed-source@@`.

## Ed25519 Certification Authority Cross-Signed Certificate {#ed25519-ca-cross-cert}

If an email client only trusts the RSA Certification Authority Root Certificate found in {{rsa-ca-cert}}, they can use this intermediate CA certificate to verify any end-entity certificate issued by the example Ed25519 Certification Authority.

{: artwork-name="ca.25519.cross.crt"}
~~~ application/x-x509-ca-cert
{::include ca.25519.cross.crt}
~~~

# Carlos's Sample Certificates

Carlos has the following information:

 - Name: `Carlos Turing`
 - email Address: `carlos@smime.example`

## Carlos's Signature Verification End-Entity Certificate {#carlos-verify-cert}

This certificate is used for verification of signatures made by Carlos.

{: artwork-name="carlos.sign.crt"}
~~~ application/x-pem-file
{::include carlos.sign.25519.crt}
~~~

## Carlos's Signing Private Key Material {#carlos-sign-key}

This private key material is used by Carlos to create signatures.

{: artwork-name="carlos.sign.key"}
~~~ application/x-pem-file
{::include carlos.sign.25519.key}
~~~

This secret key is the SHA-256 ({{SHA}}) digest of the ASCII string `@@carlos.sign.25519.seed-source@@`.

## Carlos's Encryption End-Entity Certificate {#carlos-encrypt-cert}

This certificate is used to encrypt messages to Carlos.
It contains an SMIMECapabilities extension to indicate that Carlos's MUA expects Elliptic Curve Diffie-Hellman (ECDH) with the HMAC-based Key Derivation Function (HKDF) using SHA-256, and that it uses the AES-128 key wrap algorithm, as indicated in {{RFC8418}}.

{: artwork-name="carlos.encrypt.crt"}
~~~ application/x-pem-file
{::include carlos.encrypt.25519.crt}
~~~

## Carlos's Decryption Private Key Material {#carlos-decrypt-key}

This private key material is used by Carlos to decrypt messages.

{: artwork-name="carlos.encrypt.key"}
~~~ application/x-pem-file
{::include carlos.encrypt.25519.key}
~~~

This secret key is the SHA-256 ({{SHA}}) digest of the ASCII string `@@carlos.encrypt.25519.seed-source@@`.

## PKCS #12 Object for Carlos

This PKCS #12 ({{RFC7292}}) object contains the same information as presented in {{ed25519-ca-cross-cert}}, {{carlos-verify-cert}}, {{carlos-sign-key}}, {{carlos-encrypt-cert}}, and {{carlos-decrypt-key}}.

It is locked with the simple five-letter password `carlos`.

{: artwork-name="carlos.p12"}
~~~ application/x-pem-file
{::include carlos.25519.p12}
~~~

# Dana's Sample Certificates

Dana has the following information:

 - Name: `Dana Hopper`
 - email Address: `dna@smime.example`

## Dana's Signature Verification End-Entity Certificate {#dana-verify-cert}

This certificate is used for verification of signatures made by Dana.

{: artwork-name="dana.sign.crt"}
~~~ application/x-pem-file
{::include dana.sign.25519.crt}
~~~

## Dana's Signing Private Key Material {#dana-sign-key}

This private key material is used by Dana to create signatures.

{: artwork-name="dana.sign.key"}
~~~ application/x-pem-file
{::include dana.sign.25519.key}
~~~

This secret key is the SHA-256 ({{SHA}}) digest of the ASCII string `@@dana.sign.25519.seed-source@@`.

## Dana's Encryption End-Entity Certificate {#dana-encrypt-cert}

This certificate is used to encrypt messages to Dana.
It contains an SMIMECapabilities extension to indicate that Dana's MUA expects ECDH with HKDF using SHA-256, and that it uses the AES-128 key wrap algorithm, as indicated in {{RFC8418}}.

{: artwork-name="dana.encrypt.crt"}
~~~ application/x-pem-file
{::include dana.encrypt.25519.crt}
~~~

## Dana's Decryption Private Key Material {#dana-decrypt-key}

This private key material is used by Dana to decrypt messages.

{: artwork-name="dana.encrypt.key"}
~~~ application/x-pem-file
{::include dana.encrypt.25519.key}
~~~

This seed is the SHA-256 ({{SHA}}) digest of the ASCII string `@@dana.encrypt.25519.seed-source@@`.

## PKCS #12 Object for Dana

This PKCS #12 ({{RFC7292}}) object contains the same information as presented in {{ed25519-ca-cross-cert}}, {{dana-verify-cert}}, {{dana-sign-key}}, {{dana-encrypt-cert}}, and {{dana-decrypt-key}}.

It is locked with the simple four-letter password `dana`.

{: artwork-name="dana.p12"}
~~~ application/x-pem-file
{::include dana.25519.p12}
~~~

# Security Considerations

The keys presented in this document should be considered compromised and insecure, because the secret key material is published and therefore not secret.

Any application that maintains a denylist of invalid key material should include these keys in its list.

# IANA Considerations

This document has no IANA actions.

--- back

# Acknowledgements

This document was inspired by similar work in the OpenPGP space by Bjarni Rúnar Einarsson and juga at {{I-D.bre-openpgp-samples}}.

Eric Rescorla helped spot issues with certificate formats.

Sean Turner pointed to {{RFC4134}} as prior work.

Deb Cooley suggested that Alice and Bob should have separate certificates for signing and encryption.

Wolfgang Hommel helped to build reproducible encrypted PKCS #12 objects.

Carsten Bormann got the XML `sourcecode` markup working for this document.

David A. Cooper identified problems with the certificates and suggested corrections.

Lijun Liao helped get the terminology right.

Stewart Bryant and Roman Danyliw provided editorial suggestions.

# Document Considerations

\[ RFC Editor: please remove this section before publication ]

## Document History

### Substantive Changes from draft-ietf-\*-07 to draft-ietf-\*-08

  - Apply editorial cleanup suggested during review

### Substantive Changes from draft-ietf-\*-06 to draft-ietf-\*-07

  - Correct document history
  - Restore PKCS #12 for dana and bob from -05

### Substantive Changes from draft-ietf-\*-05 to draft-ietf-\*-06

  - Added outbound references for acronyms PEM, CRL, and OCSP, thanks Stewart Bryant.
  - Accidentally modified PKCS #12 for dana and bob

### Substantive Changes from draft-ietf-\*-04 to draft-ietf-\*-05

 - Switch from SHA512 to SHA1 as MAC checksum in PKCS #12 objects, for interop with Keychain Access on macOS.

### Substantive Changes from draft-ietf-\*-03 to draft-ietf-\*-04

 - Order subject/issuer DN components by scope.
 - Put cross-signed intermediate CA certificates into PKCS #12 instead of self-signed root CA certificates.

### Substantive Changes from draft-ietf-\*-02 to draft-ietf-\*-03

 - Correct encoding of S/MIME Capabilities extension.
 - Change "Certificate Authority" to "Certification Authority".
 - Add CertificatePolicies to all intermediate and end-entity certificates.
 - Add organization and organizational unit to all certificates.

### Substantive Changes from draft-ietf-\*-01 to draft-ietf-\*-02

 - Added cross-signed certificates for both CAs
 - Added S/MIME Capabilities extension for Carlos and Dana's encryption keys, indicating preferred ECDH parameters.
 - Ensure no serial numbers are negative.
 - Encode keyUsage extensions in minimum-length BIT STRINGs.

### Substantive Changes from draft-ietf-\*-00 to draft-ietf-\*-01

 - Added Curve25519 sample certificates (new CA, Carlos, and Dana)

### Substantive Changes from draft-dkg-\*-05 to draft-ietf-\*-00

 - WG adoption (dkg moves from Author to Editor)

### Substantive Changes from draft-dkg-\*-04 to draft-dkg-\*-05

 - PEM blobs are now `sourcecode`, not `artwork`

### Substantive Changes from draft-dkg-\*-03 to draft-dkg-\*-04

 - Describe deterministic key generation
 - label PEM blobs with filenames in XML

### Substantive Changes from draft-dkg-\*-02 to draft-dkg-\*-03

 - Alice and Bob now each have two distinct certificates: one for
   signing, one for encryption, and public keys to match.

### Substantive Changes from draft-dkg-\*-01 to draft-dkg-\*-02

 - PKCS #12 objects are deliberately locked with simple passphrases

### Substantive Changes from draft-dkg-\*-00 to draft-dkg-\*-01

 - changed all three keys to use RSA instead of RSA-PSS
 - set keyEncipherment keyUsage flag instead of dataEncipherment in EE certs
