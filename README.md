S/MIME Example Keys and Certificates
------------------------------------

This repository hosts the source for the Internet Draft published at https://datatracker.ietf.org/doc/draft-dkg-lamps-samples/

It includes:

 - source for the draft
 - simple code for how the artifacts were generated, so that they can be re-generated

Note that the PKCS 12 objects need extra hackery to be deterministically generated because of the encryption that happens there, which would normally be non-deterministic. 
That encryption is done with `certtool` from [GnuTLS](https://gnutls.org), but using [faketime's FAKERANDOM capability](https://github.com/wolfcw/libfaketime/issues/275) to trick it into producing deterministic output.

Minor editorial changes can be suggested via merge requests or issues at this gitlab repository, or by e-mail to the author.

Please direct all significant commentary to the public IETF LAMPS mailing list: `spasm@ietf.org`
