#!/usr/bin/make -f
#
# Prerequisites: apt install gnutls-bin ruby-kramdown-rfc2629 xml2rfc weasyprint
#

draft = lamps-samples
OUTPUT = $(draft).txt $(draft).html $(draft).xml $(draft).pdf
dependencies = ca.rsa.key ca.rsa.crt ca.rsa.seed ca.rsa.seed-source ca.rsa.cross.crt $(foreach name,alice bob,$(name).p12 $(foreach type,sign encrypt,$(foreach format,key crt seed seed-source, $(name).$(type).$(format))))

dependencies += ca.25519.key ca.25519.crt ca.25519.seed-source ca.25519.cross.crt $(foreach name,carlos dana,$(name).25519.p12 $(foreach format,key crt seed-source,$(foreach type,sign encrypt,$(name).$(type).25519.$(format))))

certtool = certtool --no-text --key-type rsa --bits 2048

# intended to find exactly one LD_PRELOAD library
# FIXME: should warn if zero or more than one was found.
LIBFAKETIME ?= $(wildcard /usr/lib/*/faketime/libfaketime.so.1)

DATE ?= "2019-11-20 06:54:18Z"
CURVE_DATE ?= "2020-12-15 21:35:44Z"

all: $(OUTPUT)

$(draft).md: $(draft).in.md assemble $(dependencies)
	./assemble < $< >$@.tmp
	mv $@.tmp $@

%.xmlv2: %.md
	kramdown-rfc2629 --v3 < $< > $@.tmp
	mv $@.tmp $@

%.xml: %.xmlv2
	xml2rfc --v2v3 -o $@ $<

%.html: %.xml
	xml2rfc $< --html

%.pdf: %.xml
	xml2rfc $< --pdf

%.txt: %.xml
	xml2rfc $< --text

%.sign.template: %.prefix sign.template
	cat $^ >$@
%.encrypt.template: %.prefix encrypt.template
	cat $^ >$@

%.sign.25519.template: %.25519.prefix sign.25519.template
	cat $^ >$@
%.encrypt.25519.template: %.25519.prefix encrypt.25519.template
	cat $^ >$@

# making a deterministic template
%.template.det: %.template
	printf 'serial = 0x'  > $@.tmp
	./generate-serial-number < $< >> $@.tmp
	cat $< >> $@.tmp
	mv $@.tmp $@

%.seed-source:
	printf draft-lamps-sample-certs-keygen.%s.seed $* > $@
%.seed: %.seed-source
	sha256sum $< | cut -b1-56 > $@

%.key: %.seed
	$(certtool) --generate-privkey --provable --seed $(shell cat $<) --outfile $@

%.25519.key: %.25519.seed-source
	./generate-25519.py > $@.tmp < $<
	mv $@.tmp $@
%.encrypt.25519.key: %.encrypt.25519.seed-source
	./generate-25519.py x25519 > $@.tmp < $<
	mv $@.tmp $@

ca.25519.cross.crt: ca.25519.pubkey ca.rsa.key ca.rsa.crt ca.25519.cross.template.det
	certtool --no-text --generate-certificate --load-pubkey ca.25519.pubkey --template ca.25519.cross.template.det --load-ca-certificate ca.rsa.crt --load-ca-privkey ca.rsa.key --outfile $@

ca.rsa.cross.crt: ca.rsa.pubkey ca.25519.key ca.25519.crt ca.rsa.cross.template.det
	certtool --no-text --generate-certificate --load-pubkey ca.rsa.pubkey --template ca.rsa.cross.template.det --load-ca-certificate ca.25519.crt --load-ca-privkey ca.25519.key --outfile $@

ca.25519.crt: ca.25519.key ca.25519.template.det
	certtool --no-text --generate-self-signed --load-privkey ca.25519.key --template ca.25519.template.det --outfile $@

ca.rsa.crt: ca.rsa.key ca.rsa.template.det
	$(certtool) --hash SHA512 --generate-self-signed --load-privkey ca.rsa.key --template ca.rsa.template.det --outfile $@

%.crt: %.template.det %.pubkey ca.rsa.key ca.rsa.crt
	$(certtool) --hash SHA512 --generate-certificate --load-pubkey $*.pubkey --template $*.template.det --load-ca-certificate ca.rsa.crt --load-ca-privkey ca.rsa.key --outfile $@

%.pubkey: %.key
	certtool --pubkey-info --outfile $@ --load-privkey $<

%.25519.crt: %.25519.template.det %.25519.pubkey ca.25519.key ca.25519.crt
	certtool --no-text --generate-certificate --load-pubkey $*.25519.pubkey --template $*.25519.template.det --load-ca-certificate ca.25519.crt --load-ca-privkey ca.25519.key --outfile $@

%.both.key: %.encrypt.key %.sign.key
	cat $^ > $@.tmp
	mv $@.tmp $@

%.both.crt: %.encrypt.crt %.sign.crt
	cat $^ > $@.tmp
	mv $@.tmp $@

digests = SHA1 SHA256 SHA384 SHA512
unimp_digests =  RMD160 SHA3-224 SHA3-256 SHA3-384 SHA3-512
ciphers = 3des 3des-pkcs12 aes-128 aes-192 aes-256 rc2-40 arcfour

%.test.p12: $(foreach user,alice bob carlos.25519 dana.25519,$(user).both.key $(user).both.crt) $(foreach ca,rsa 25519,ca.$(ca).cross.crt)
	./pkcs12-generate $@

PKCS12_tests = $(foreach user,alice bob carlos dana,$(foreach digest,$(digests),$(foreach cipher,$(ciphers),$(user).$(digest).$(cipher).test.p12)))

p12.zip: $(PKCS12_tests)
	zip -r $@ $^

p12tools = certtool openssl keytool pk12util

%.p12.out/certtool.out: %.p12
	mkdir -p $*.p12.out
	certtool --p12-info --inder --password $(firstword $(subst ., ,$@)) < $< --outfile $@.tmp
	mv $@.tmp $@

%.p12.out/keytool.out: %.p12
	mkdir -p $*.p12.out
	keytool -keystore $< -storepass $(firstword $(subst ., ,$@)) -v -list > $@.tmp
	mv $@.tmp $@

%.p12.out/pk12util.out: %.p12
	mkdir -p $*.p12.out
	pk12util -l $< -W $(firstword $(subst ., ,$@)) > $@.tmp
	mv $@.tmp $@

%.p12.out/openssl.out: %.p12
	mkdir -p $*.p12.out
	openssl pkcs12 -passin pass:$(firstword $(subst ., ,$@)) -passout pass:$(firstword $(subst ., ,$@)) < $< -out $@

%.p12.out/stamp: %.p12 $(foreach tool,$(p12tools),%.p12.out/$(tool).out)
	touch $@

p12-test-summary.zip: $(PKCS12_tests) $(foreach tool,$(p12tools),$(foreach p12,$(PKCS12_tests),$(p12).out/$(tool).out))
	zip -r $@ $^

%.p12: %.both.key %.both.crt ca.rsa.cross.crt
	FAKERANDOM_SEED="0x$(shell printf lamps-samples-random-seed-%s '$*' | sha256sum - | cut -c1-16)" FAKETIME=$(DATE) LD_PRELOAD=$(LIBFAKETIME) $(certtool) --to-p12 --p12-name $* --password=$* --load-privkey $*.both.key --load-ca-certificate ca.rsa.cross.crt --load-certificate $*.both.crt --outfile $@

%.both.25519.key: %.encrypt.25519.key %.sign.25519.key
	cat $^ > $@.tmp
	mv $@.tmp $@

%.both.25519.crt: %.encrypt.25519.crt %.sign.25519.crt
	cat $^ > $@.tmp
	mv $@.tmp $@

%.25519.p12: %.both.25519.key %.both.25519.crt ca.25519.cross.crt
	FAKERANDOM_SEED="0x$(shell printf lamps-samples-random-seed-%s '$*' | sha256sum - | cut -c1-16)" FAKETIME=$(CURVE_DATE) LD_PRELOAD=$(LIBFAKETIME) $(certtool) --to-p12 --p12-name $* --password=$* --load-privkey $*.both.25519.key --load-ca-certificate ca.25519.cross.crt --load-certificate $*.both.25519.crt --outfile $@

gpghome: bob.p12
	rm -rf $@ $@.tmp
	mkdir $@.tmp
	echo bob > $@.tmp/passwd
	gpgsm --disable-dirmngr --homedir $@.tmp --pinentry-mode loopback --batch --passphrase-fd 4 4<$@.tmp/passwd --import bob.p12
	mv $@.tmp $@

%.bin.p12: %.p12
	grep -v '^-' < $< > $@.noheader.tmp
	base64 -d  < $@.noheader.tmp > $@.tmp
	rm -f $@.noheader.tmp
	mv $@.tmp $@

%.urlencoded: %.crt
	printf b64cert= > $@.tmp
	urlencode "$$(cat $<)" >> $@.tmp
	mv $@.tmp $@

%.lint: %.urlencoded
	wget --post-file=$< -O $@ https://crt.sh/lintcert

lint-report.txt: $(sort $(patsubst %.crt,%.lint,$(filter %.crt,$(dependencies))))
	head -v $^ > $@

clean:
	-rm -f $(OUTPUT) $(draft).xmlv2 $(draft).md *.sign.template *.encrypt.template *.template.det *.p12 *.crt *.key *.seed *.seed-source *.sign.25519.template *.pubkey *.test.p12.out/*.out *.test.p12.out/*.out.tmp

.PHONY: clean all
.SECONDARY: $(draft).md $(draft).xmlv2 $(dependencies)
