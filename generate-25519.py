#!/usr/bin/python3

from sys import stdin,argv
from hashlib import sha256
from base64 import b64encode

# SEQUENCE { INTEGER 0 SEQUENCE { OID curveEd25519 (1 3 101 112) } OCTET STRING 04 20 ...
edprefix = b'\x30\x2e\x02\x01\x00\x30\x05\x06\x03\x2b\x65\x70\x04\x22\x04\x20'

# SEQUENCE { INTEGER 0 SEQUENCE { OID curveX25519 (1 3 101 110) } OCTET STRING 04 20 ...
kxprefix = b'\x30\x2e\x02\x01\x00\x30\x05\x06\x03\x2b\x65\x6e\x04\x22\x04\x20'

prefix=edprefix
if len(argv) > 1 and argv[1] == 'x25519':
    prefix=kxprefix

rr = sha256(stdin.read().encode()).digest()
print('-----BEGIN PRIVATE KEY-----\n' + b64encode(prefix + rr).decode() + '\n-----END PRIVATE KEY-----')
